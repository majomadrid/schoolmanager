<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school1=DB::table('schools')->insertGetId([
            'name' => 'Salesianos Paseo Extremadura',
            'address' => 'C/ Repulles y Vargas, 11',
            'city' => 'Madrid',
            'postalcode'=> '28011',
            'phonenumber' => '91 464 00 50',
            'homepage' => 'https://www.salesianospaseo.com',
        ]);

        $school2=DB::table('schools')->insertGetId([
            'name' => 'Deutsche Schule Madrid',
            'address' => 'C/ Monasterio de Guadalupe, 7',
            'city' => 'Madrid',
            'postalcode'=> '28049',
            'phonenumber' => '91 558 02 00',
            'homepage' => 'https://dsmadrid.org',
        ]);

        DB::table('teachers')->insert([
            'firstname' => 'Alberto',
            'lastname' => 'Blanco Morales',
            'address' => 'Paseo Extremadura, 120',
            'city' => 'Madrid',
            'postalcode'=> '28011',
            'phonenumber' => '91 463 21 22',
            'email' => 'albertobm@gmail.com',
            'school_id' => $school1,
        ]);

        DB::table('teachers')->insert([
            'firstname' => 'Juan',
            'lastname' => 'Laguna Meta',
            'address' => 'C/ Isaak Peral, 120',
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 634 34 76',
            'email' => 'jlm100@gmail.com',
            'school_id' => $school1,
        ]);

        DB::table('teachers')->insert([
            'firstname' => str_random(6),
            'lastname' => str_random(13),
            'address' => 'C/ '.str_random(),
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 '.random_int(1000000,9999999),
            'email' => str_random(10).'@gmail.com',
            'school_id' => $school2,
        ]);

        DB::table('teachers')->insert([
            'firstname' => str_random(6),
            'lastname' => str_random(13),
            'address' => 'C/ '.str_random(),
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 '.random_int(1000000,9999999),
            'email' => str_random(10).'@gmail.com',
            'school_id' => $school2,
        ]);

        DB::table('teachers')->insert([
            'firstname' => str_random(6),
            'lastname' => str_random(13),
            'address' => 'C/ '.str_random(),
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 '.random_int(1000000,9999999),
            'email' => str_random(10).'@gmail.com',
            'school_id' => $school2,
        ]);

        DB::table('students')->insert([
            'firstname' => str_random(6),
            'lastname' => str_random(13),
            'address' => 'C/ '.str_random(),
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 '.random_int(1000000,9999999),
            'email' => str_random(10).'@gmail.com',
            'class' => '10a',
            'school_id' => $school1,
        ]);
        DB::table('students')->insert([
            'firstname' => str_random(6),
            'lastname' => str_random(13),
            'address' => 'C/ '.str_random(),
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 '.random_int(1000000,9999999),
            'email' => str_random(10).'@gmail.com',
            'class' => '11a',
            'school_id' => $school1,
        ]);
        DB::table('students')->insert([
            'firstname' => str_random(6),
            'lastname' => str_random(13),
            'address' => 'C/ '.str_random(),
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 '.random_int(1000000,9999999),
            'email' => str_random(10).'@gmail.com',
            'class' => '12a',
            'school_id' => $school1,
        ]);

        DB::table('students')->insert([
            'firstname' => str_random(6),
            'lastname' => str_random(13),
            'address' => 'C/ '.str_random(),
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 '.random_int(1000000,9999999),
            'email' => str_random(10).'@gmail.com',
            'class' => '10',
            'school_id' => $school2,
        ]);
        DB::table('students')->insert([
            'firstname' => str_random(6),
            'lastname' => str_random(13),
            'address' => 'C/ '.str_random(),
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 '.random_int(1000000,9999999),
            'email' => str_random(10).'@gmail.com',
            'class' => '11',
            'school_id' => $school2,
        ]);
        DB::table('students')->insert([
            'firstname' => str_random(6),
            'lastname' => str_random(13),
            'address' => 'C/ '.str_random(),
            'city' => 'Madrid',
            'postalcode'=> '28002',
            'phonenumber' => '91 '.random_int(1000000,9999999),
            'email' => str_random(10).'@gmail.com',
            'class' => '12',
            'school_id' => $school2,
        ]);


    }
}
