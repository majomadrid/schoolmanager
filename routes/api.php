<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('school','Api\SchoolController');

Route::apiResource('school/{school}/teacher','Api\TeacherController');

Route::apiResource('school/{school}/student','Api\StudentController');