@extends('baselayout')

@section('content')
    <div class="row">
        <div class="col">
            <h4><a href="/"><i class="fas fa-angle-left"></i></a>&nbsp;{{ $school->name }}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#profes" role="tab" aria-controls="home" aria-selected="true">Profesores</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#alumnos" role="tab" aria-controls="profile" aria-selected="false">Alumnos</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="profes" role="tabpanel" aria-labelledby="profes-tab">
                    <div class="row">
                        <div class="col">
                            <table class="table-sm table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellidos</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Nº de teléfono</th>
                                    <th scope="col">Dirección</th>
                                    <th scope="col">Código postal</th>
                                    <th scope="col">Ciudad</th>
                                    <th scope="col">Editar</th>
                                    <th scope="col">Borrar</th>
                                </tr>
                                </thead>
                                <tbody id="teacherlist">
                                <tr><td class="text-center" colspan="6"><img src="{{ asset('img/123.gif') }}"></td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createTeacherModal">Añadir profesor</button>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="alumnos" role="tabpanel" aria-labelledby="alumnos-tab">
                    <div class="row">
                        <div class="col">
                            <table class="table-sm table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellidos</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Nº de teléfono</th>
                                    <th scope="col">Clase</th>
                                    <th scope="col">Dirección</th>
                                    <th scope="col">Código postal</th>
                                    <th scope="col">Ciudad</th>
                                    <th scope="col">Editar</th>
                                    <th scope="col">Borrar</th>
                                </tr>
                                </thead>
                                <tbody id="studentlist">
                                <tr><td class="text-center" colspan="7"><img src="{{ asset('img/123.gif') }}"></td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createStudentModal">Añadir alumno</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Create Teacher Modal -->
    <div class="modal fade" id="createTeacherModal" tabindex="-1" role="dialog" aria-labelledby="createTeacherModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createTeacherModalLabel">Añadir profesor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="createteacherform">
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="firstname" id="createteacherfirstname" placeholder="Nombre" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="lastname" id="createteacherlastname" placeholder="Apellidos" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="email" id="createteacheremail" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="address" placeholder="Dirección">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="postalcode" placeholder="Código postal">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="city" placeholder="Ciudad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="phonenumber" placeholder="Nº de teléfono">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="createteacher">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Teacher Modal -->
    <div class="modal fade" id="editTeacherModal" tabindex="-1" role="dialog" aria-labelledby="editTeacherModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editTeacherModalLabel">Editar profesor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editteacherform">
                        {{method_field('PUT')}}
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="firstname" placeholder="Nombre" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="lastname" placeholder="Apellidos" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="email" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="address" placeholder="Dirección">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="postalcode" placeholder="Código postal">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="city" placeholder="Ciudad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="phonenumber" placeholder="Nº de teléfono">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="updateteacher" data-teacher="">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Create Student Modal -->
    <div class="modal fade" id="createStudentModal" tabindex="-1" role="dialog" aria-labelledby="createStudentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createStudentModalLabel">Añadir alumno</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="createstudentform">
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="firstname" id="createstudentfirstname" placeholder="Nombre" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="lastname" id="createstudentlastname" placeholder="Apellidos" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="email" id="createstudentemail" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="address" placeholder="Dirección">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="postalcode" placeholder="Código postal">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="city" placeholder="Ciudad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="phonenumber" placeholder="Nº de teléfono">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="class" placeholder="clase">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="createstudent">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Student Modal -->
    <div class="modal fade" id="editStudentModal" tabindex="-1" role="dialog" aria-labelledby="editStudentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editStudentModalLabel">Editar alumno</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editstudentform">
                        {{method_field('PUT')}}
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="firstname" placeholder="Nombre" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="lastname" placeholder="Apellidos" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="email" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="address" placeholder="Dirección">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="postalcode" placeholder="Código postal">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="city" placeholder="Ciudad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="phonenumber" placeholder="Nº de teléfono">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="class" placeholder="clase">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="updatestudent" data-student="">Guardar</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Delete Teacher Modal -->
    <div class="modal fade" id="deleteTeacherModal" tabindex="-1" role="dialog" aria-labelledby="deleteTeacherModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteTeacherModalLabel">Borrar profesor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            ¿Estás seguro que quieres borrar <span id="teachername"></span>?
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-danger" id="deleteteacher" data-teacher="">Borrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Student Modal -->
    <div class="modal fade" id="deleteStudentModal" tabindex="-1" role="dialog" aria-labelledby="deleteStudentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteStudentModalLabel">Borrar alumno</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            ¿Estás seguro que quieres borrar <span id="studentname"></span>?
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-danger" id="deletestudent" data-student="">Borrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        let teachers=[];
        let students=[];

        $(document).ready(function(){
            getteachers();
            getstudents();

            $('#createteacher').click(function(event) {
                if(document.forms['createteacherform'].reportValidity()) {
                    axios.post('/api/school/{{$school->id}}/teacher', $('#createteacherform').serialize())
                        .then(function (response) {
                            $('#createTeacherModal .close').trigger('click');
                            $('#createteacherform input').val('');
                            getteachers();
                        });
                }
            });

            $('#createstudent').click(function(event) {
                if(document.forms['createstudentform'].reportValidity()) {
                    axios.post('/api/school/{{ $school->id }}/student', $('#createstudentform').serialize())
                        .then(function (response) {
                            $('#createStudentModal .close').trigger('click');
                            $('#createstudentform input').val('');
                            getstudents();
                        });
                }
            });
        });

        function getteachers() {
            axios.get('/api/school/{{ $school->id }}/teacher')
                .then(function (response) {
                    teachers=response.data;
                    showteacherlist();
                });
        }

        function showteacherlist() {
            let items = [];
            if(teachers.length==0) {
                items.push("<tr><td colspan='9'>No existen profesores en nuestra base de datos todavía.</td></tr>");
            } else {
                $.each(teachers, function (i, val) {
                    $.each(val, function (j, v) {
                        if (v == null) val[j] = '';
                    });
                    items.push("<tr><td>" + val.firstname + "</td><td>" + val.lastname + "</td><td>" + val.email + "</td><td>" + val.phonenumber + "</td>");
                    items.push("<td>" + val.address + "</td><td>" + val.postalcode + "</td><td>" + val.city + "</td>");
                    items.push("<td><p class='center-block' data-placement='top' data-toggle='tooltip' title='Editar'><button class='btn btn-primary btn-xs showteacheredit' data-title='Editar' data-toggle='modal' data-target='#editTeacherModal' data-teacher='" + val.id + "'><i class=\"fas fa-edit\"></i></button></p></td>");
                    items.push("<td><p class='center-block' data-placement='top' data-toggle='tooltip' title='Borrar'><button class='btn btn-danger btn-xs showteacherdelete' data-title='Borrar' data-toggle='modal' data-target='#deleteTeacherModal' data-teacher='" + val.id + "'><i class=\"fas fa-trash-alt\"></i></button></p>");
                    items.push("</tr>");
                });
            }

            $("#teacherlist").html(items.join(""));


            $('.showteacherdelete').click(function (e) {
                $('#deleteteacher').data('teacher',$(this).data('teacher'));
                axios.get('/api/school/{{$school->id}}/teacher/'+$(this).data('teacher'))
                    .then(function (response) {
                        teacher=response.data;
                        $('#teachername').html(teacher.firstname+" "+teacher.lastname);
                    });

            });

            $('.showteacheredit').click(function (e) {
                axios.get('/api/school/{{$school->id}}/teacher/'+$(this).data('teacher'))
                    .then(function (response) {
                        teacher=response.data;
                        $('#updateteacher').data('teacher',teacher.id);
                        $.each(teacher,function(attr,val){
                            if(val==null) val='';
                            $('#editteacherform input[name="'+attr+'"]').val(val);
                        });
                    });

            });
        }

        $('#deleteteacher').click(function(e) {
            axios.post('/api/school/{{$school->id}}/teacher/'+$(this).data('teacher'),{'_method': 'DELETE'})
                .then(function (response) {
                    success=response.data.success;
                    if(success) {
                        $('#deleteTeacherModal .close').trigger('click');
                        getteachers();
                    } else {
                        $('#deleteTeacherModal .modal-body .col').html('Error al borrar el profesor');
                    }
                });

        });

        $('#updateteacher').click(function(e){
            if(document.forms['editteacherform'].reportValidity()) {
                axios.post('/api/school/{{$school->id}}/teacher/' + $(this).data('teacher'), $('#editteacherform').serialize())
                    .then(function (response) {
                        success = response.data.success;
                        if (success) {
                            $('#editTeacherModal .close').trigger('click');
                            getteachers();
                        } else {
                            $('#editTeacherModal .modal-body .col').html('Error al actualizar datos del profesor');
                        }
                    });
            }
        });

        function getstudents() {
            axios.get('/api/school/{{ $school->id }}/student')
                .then(function (response) {
                    students=response.data;
                    showstudentlist();
                });
        }

        function showstudentlist() {
            let items = [];
            if(students.length==0) {
                items.push("<tr><td colspan='7'>No existen alumnos en nuestra base de datos todavía.</td></tr>");
            } else {
                $.each(students, function (i, val) {
                    $.each(val, function (j, v) {
                        if (v == null) val[j] = '';
                    });
                    items.push("<tr><td>" + val.firstname + "</td><td>" + val.lastname + "</td><td>" + val.email + "</td><td>" + val.phonenumber + "</td><td>" + val.class + "</td>");
                    items.push("<td>" + val.address + "</td><td>" + val.postalcode + "</td><td>" + val.city + "</td>");
                    items.push("<td><p class='center-block' data-placement='top' data-toggle='tooltip' title='Editar'><button class='btn btn-primary btn-xs showstudentedit' data-title='Editar' data-toggle='modal' data-target='#editStudentModal' data-student='" + val.id + "'><i class=\"fas fa-edit\"></i></button></p></td>");
                    items.push("<td><p class='center-block' data-placement='top' data-toggle='tooltip' title='Borrar'><button class='btn btn-danger btn-xs showstudentdelete' data-title='Borrar' data-toggle='modal' data-target='#deleteStudentModal' data-student='" + val.id + "'><i class=\"fas fa-trash-alt\"></i></button></p>");
                    items.push("</tr>");
                });
            }

            $("#studentlist").html(items.join(""));


            $('.showstudentdelete').click(function (e) {
                $('#deletestudent').data('student',$(this).data('student'));
                axios.get('/api/school/{{$school->id}}/student/'+$(this).data('student'))
                    .then(function (response) {
                        student=response.data;
                        $('#studentname').html(student.firstname+" "+student.lastname);
                    });
            });

            $('.showstudentedit').click(function (e) {
                axios.get('/api/school/{{$school->id}}/student/'+$(this).data('student'))
                    .then(function (response) {
                        student=response.data;
                        $('#updatestudent').data('student',student.id);
                        $.each(student,function(attr,val){
                            if(val==null) val='';
                            $('#editstudentform input[name="'+attr+'"]').val(val);
                        });
                    });

            });
        }

        $('#deletestudent').click(function(e) {
            axios.post('/api/school/{{$school->id}}/student/'+$(this).data('student'),{'_method': 'DELETE'})
                .then(function (response) {
                    success=response.data.success;
                    if(success) {
                        $('#deleteStudentModal .close').trigger('click');
                        getstudents();
                    } else {
                        $('#deleteStudentModal .modal-body .col').html('Error al borrar el alumno');
                    }
                });

        });

        $('#updatestudent').click(function(e){
            if(document.forms['editstudentform'].reportValidity()) {
                axios.post('/api/school/{{$school->id}}/student/' + $(this).data('student'), $('#editstudentform').serialize())
                    .then(function (response) {
                        success = response.data.success;
                        if (success) {
                            $('#editStudentModal .close').trigger('click');
                            getstudents();
                        } else {
                            $('#editStudentModal .modal-body .col').html('Error al actualizar datos del alumno');
                        }
                    });
            }
        });


    </script>
@endsection