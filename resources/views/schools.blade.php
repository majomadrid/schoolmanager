@extends('baselayout')

@section('content')
    <div class="row">
        <div class="col-md-11 offset-md-1">
            <table class="table-sm table-striped">
                <thead>
                <tr>
                    <th scope="col">Nombre del colegio</th>
                    <th scope="col">Dirección</th>
                    <th scope="col">Código postal</th>
                    <th scope="col">Ciudad</th>
                    <th scope="col">Nº de teléfono</th>
                    <th scope="col">Editar</th>
                    <th scope="col">Borrar</th>
                </tr>
                </thead>
                <tbody id="schoolslist">
                    <tr><td class="text-center" colspan="7"><img src="{{ asset('img/123.gif') }}"></td></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-11 offset-md-1">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">Añadir colegio</button>

        </div>
    </div>

    <!-- Create Modal -->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel">Crear colegio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="createschoolform">
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="name" id="createname" placeholder="Nombre del colegio" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="address" placeholder="Dirección">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="postalcode" placeholder="Código postal">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="city" placeholder="Ciudad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="phonenumber" placeholder="Nº de teléfono">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="homepage" placeholder="Página web">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="createschool">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Editar colegio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editschoolform">
                        {{method_field('PUT')}}
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="name" placeholder="Nombre del colegio" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="address" placeholder="Dirección">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="postalcode" placeholder="Código postal">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="city" placeholder="Ciudad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="phonenumber" placeholder="Nº de teléfono">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="homepage" placeholder="Página web">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="updateschool" data-school="">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Borrar colegio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            ¿Estás seguro que quieres borrar el colegio <span id="schoolname"></span>?
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-danger" id="deleteschool" data-school="">Borrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>

        let schools=[];

        $(document).ready(function(){
            getschools();
            $('#createschool').click(function(event) {
                if(document.forms['createschoolform'].reportValidity()) {
                    axios.post('/api/school', $('#createschoolform').serialize())
                        .then(function (response) {
                            $('#createModal .close').trigger('click');
                            $('#createschoolform input').val('');
                            getschools();
                        });
                }
            });
        });

        function getschools() {
            axios.get('/api/school')
                .then(function (response) {
                    schools=response.data;
                    showlist();
                });
        }

        function showlist() {
            let items = [];
            if(schools.length==0) {
                items.push("<tr><td colspan='7'>No existen colegios en nuestra base de datos todavía.</td></tr>");
            } else {
                $.each(schools, function (i, val) {
                    $.each(val, function (j, v) {
                        if (v == null) val[j] = '';
                    });
                    items.push("<tr><td><a href='/school/" + val.id + "/members'>" + val.name + "</a></td><td>" + val.address + "</td><td>" + val.postalcode + "</td><td>" + val.city + "</td><td>" + val.phonenumber + "</td>");
                    items.push("<td><p class='center-block' data-placement='top' data-toggle='tooltip' title='Editar'><button class='btn btn-primary btn-xs showedit' data-title='Editar' data-toggle='modal' data-target='#editModal' data-school='" + val.id + "'><i class=\"fas fa-edit\"></i></button></p></td>");
                    items.push("<td><p class='center-block' data-placement='top' data-toggle='tooltip' title='Borrar'><button class='btn btn-danger btn-xs showdelete' data-title='Borrar' data-toggle='modal' data-target='#deleteModal' data-school='" + val.id + "'><i class=\"fas fa-trash-alt\"></i></button></p></td>");
                    items.push("</tr>");
                });
            }

            $("#schoolslist").html(items.join(""));

            $('.showdelete').click(function (e) {
                $('#deleteschool').data('school',$(this).data('school'));
                axios.get('/api/school/'+$(this).data('school'))
                    .then(function (response) {
                        school=response.data;
                        $('#schoolname').html(school.name);
                    });

            });

            $('.showedit').click(function (e) {
                axios.get('/api/school/'+$(this).data('school'))
                    .then(function (response) {
                        school=response.data;
                        $('#updateschool').data('school',school.id);
                        $.each(school,function(attr,val){
                            if(val==null) val='';
                            $('#editschoolform input[name="'+attr+'"]').val(val);
                        });
                    });

            });
        }

        $('#deleteschool').click(function(e) {
            axios.post('/api/school/'+$(this).data('school'),{'_method': 'DELETE'})
                .then(function (response) {
                    success=response.data.success;
                    console.log(success);
                    if(success) {
                        $('#deleteModal .close').trigger('click');
                        getschools();
                    } else {
                        $('#deleteModal .modal-body .col').html('Error al borrar el colegio');
                    }
                });

        });

        $('#updateschool').click(function(e){
            if(document.forms['editschoolform'].reportValidity()) {
                axios.post('/api/school/' + $(this).data('school'), $('#editschoolform').serialize())
                    .then(function (response) {
                        success = response.data.success;
                        if (success) {
                            $('#editModal .close').trigger('click');
                            getschools();
                        } else {
                            $('#editModal .modal-body .col').html('Error al editar el colegio');
                        }
                    });
            }
        });


    </script>
@endsection
