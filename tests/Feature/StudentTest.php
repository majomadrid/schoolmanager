<?php

namespace Tests\Feature;

use App\School;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StudentTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $school=School::query()->first();

        $response = $this->json('POST','/api/school/'.$school->id.'/student',['firstname'=>'Test','lastname'=>'Apellido']);

        $response->assertStatus(200)->assertJsonStructure(['firstname','lastname']);
    }
}
