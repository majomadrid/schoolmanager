<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'address', 'city', 'postalcode', 'phonenumber', 'email', 'school_id'
    ];

    public function school() {
        return $this->belongsTo('App\School');
    }
}
