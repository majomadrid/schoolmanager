<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'address', 'city', 'postalcode', 'phonenumber', 'email', 'class', 'school_id'
    ];

    public function school() {
        return $this->belongsTo('App\School');
    }
}
