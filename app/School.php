<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'name', 'address', 'city', 'postalcode', 'phonenumber', 'homepage'
    ];

    public function teachers() {
        return $this->hasMany('App\Teacher');
    }

    public function students() {
        return $this->hasMany('App\Student');
    }
}
