<?php

namespace App\Http\Controllers\Api;

use App\School;
use App\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(School $school)
    {
        $teacher=Teacher::where('school_id',$school->id)->orderBy('lastname')->get();

        return $teacher;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(School $school, Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
        ]);

        $data=$request->all();
        $data['school_id']=$school->id;
        $teacher=Teacher::create($data);

        return $teacher;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(School $school,Teacher $teacher)
    {

        return $teacher;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,School $school, Teacher $teacher)
    {
        $status=true;
        try {
            $teacher->update($request->all());
        } catch (\Exception $e) {
            $status=false;
        }

        return response()->json(['success'=>$status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school, Teacher $teacher)
    {
        $status=$teacher->delete();

        return response()->json(['success'=>$status]);
    }
}
