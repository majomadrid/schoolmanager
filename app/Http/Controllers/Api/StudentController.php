<?php

namespace App\Http\Controllers\Api;

use App\School;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\School $school
     * @return \Illuminate\Http\Response
     */
    public function index(School $school)
    {
        $students=Student::where('school_id',$school->id)->orderBy('lastname')->get();

        return $students;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\School $school
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(School $school, Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
        ]);

        $data=$request->all();
        $data['school_id']=$school->id;
        $student=Student::create($data);

        return $student;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(School $school, Student $student)
    {

        return $student;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,School $school, Student  $student)
    {
        $status=true;
        try {
            $student->update($request->all());
        } catch (\Exception $e) {
            $status=false;
        }

        return response()->json(['success'=>$status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school, Student $student)
    {
        $status=$student->delete();

        return response()->json(['success'=>$status]);
    }
}
